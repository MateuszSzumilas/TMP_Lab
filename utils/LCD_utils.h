/*
 * LCD_utils.h
 *
 *  Created on: 27.02.2018
 *      Author: M. Szumilas
 */

#ifndef LCD_UTILS_H_
#define LCD_UTILS_H_

#include <ti/grlib/grlib.h>

#define LCD_TERM_MARGIN 4   /* margin around the printed text (in px) */

int LCD_term_init (Graphics_Context* ctx);
int LCD_term_print (char* txt);

#endif /* LCD_UTILS_H_ */
